#!/bin/sh

set -x

# Grab our libs
BINDIR=`dirname $0`
. "$BINDIR/setup-lib.sh"

if [ -f $OURDIR/hibench-done ]; then
    exit 0
fi

logtstart "hibench"
CURDIR=`pwd`
git clone https://gitlab.com/mrkatebzadeh/k8s-hibench-images $HOME/k8s-bigdata
cd $HOME/k8s-bigdata
#./build.sh
cd $CURDIR

logtend "hibench"
touch $OURDIR/hibench-done
