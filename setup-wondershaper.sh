#!/bin/sh

set -x

# Grab our libs
BINDIR=`dirname $0`
. "$BINDIR/setup-lib.sh"

if [ -f $OURDIR/wondershaper-done ]; then
    exit 0
fi

logtstart "wondershaper"
CURDIR=`pwd`
git clone https://github.com/magnific0/wondershaper.git $HOME/wondershaper
cd $HOME/wondershaper
sudo make install
cd $CURDIR
logtend "wondershaper"
touch $OURDIR/wondershaper-done

